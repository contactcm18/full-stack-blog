export default req => {
  const cookies = req.headers.cookie;
  if (cookies) {
    const parsedCookies = {};

    const matches = cookies.match(/(\w+)=(\w+)/g);
    matches.forEach(match => {
      const split = match.split("=");
      parsedCookies[split[0]] = split[1];
    });

    req.cookies = parsedCookies;
  }
};
