import { isAuthenticated } from "~/lib/auth";

export default async ({ store, redirect }) => {
  // Called on server on initial load.
  // In store, the server has already determined if we are authenticated.
  // So, all we have to do if check if the user exists.
  if (process.server) {
    if (!store.state.auth.user) return redirect("/login");
    return;
  }

  const response = await isAuthenticated(store.commit);
  if (response.status !== 200) {
    return redirect("/login");
  }
};
