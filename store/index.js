import { isAuthenticated, relayResCookiesToClient } from "~/lib/auth";
import { getCategories } from "~/lib/posts";

export const state = () => ({});

export const mutations = {};

export const actions = {
  async nuxtServerInit({ commit }, { req, res }) {
    try {
      const response = await isAuthenticated(commit, req.headers.cookie);
      relayResCookiesToClient(response, res);

      // Get categories, set to store
      const categories = await getCategories();
      commit("posts/setCategories", categories);
      categories.forEach(category => {
        commit("posts/setNewPosts", {
          category: category.title,
          posts: []
        });
        commit("posts/setOldPosts", {
          category: category.title,
          posts: []
        });
      });
    } catch (error) {
      console.error(error);
    }
  }
};
