export const state = () => ({
  categories: [],
  newPosts: {},
  oldPosts: {}
});

export const mutations = {
  setCategories(state, categories) {
    state.categories = categories;
  },

  setNewPosts(state, { category, posts }) {
    state.newPosts[category] = posts;
  },
  appendNewPosts(state, { category, posts }) {
    state.newPosts[category] = [...state.newPosts[category], ...posts];
  },
  appendNewPost(state, post) {
    if (!state.newPosts[post.category].length) return;
    state.newPosts[post.category] = [post, ...state.newPosts[post.category]];
  },
  removeNewPost(state, post) {
    state.newPosts[post.category] = state.newPosts[post.category].filter(
      p => p.slug !== post.slug
    );
  },

  setOldPosts(state, { category, posts }) {
    state.oldPosts[category] = posts;
  },
  appendOldPosts(state, { category, posts }) {
    state.oldPosts[category] = [...state.oldPosts[category], ...posts];
  },
  appendOldPost(state, post) {
    if (!state.oldPosts[post.category].length) return;
    state.oldPosts[post.category] = [...state.oldPosts[post.category], post];
  },
  removeOldPost(state, post) {
    state.oldPosts[post.category] = state.oldPosts[post.category].filter(
      p => p.slug !== post.slug
    );
  }
};
