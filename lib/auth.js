export const isAuthenticated = async (commit, cookies = null) => {
  return new Promise(function(resolve, reject) {
    fetch(`${process.env.apiHost}/auth/isAuthenticated`, {
      method: "POST",
      mode: "cors",
      credentials: "include",
      ...(cookies && {
        headers: {
          Cookie: cookies
        }
      })
    })
      .then(async response => {
        if (response.status !== 200) {
          commit("auth/setUser", null);
        } else {
          const responseJson = await response.json();
          commit("auth/setUser", responseJson);
        }
        resolve(response);
      })
      .catch(() => reject());
  });
};

/**
 * When a fetch request is made and a response is recieved,
 * relay the response cookies back to the client.
 * Server makes request -> Receives cookies from request -> Relays them to client
 * @param response The response from fetch.
 * @param res The res on ssr that is sent back to the client.
 */
export const relayResCookiesToClient = (response, res) => {
  const cookies = response.headers.get("set-cookie");
  if (cookies) {
    const regexMatch = /(accessToken.*),\s(refreshToken.*)/g.exec(cookies);
    const cookiesToArr = [regexMatch[1], regexMatch[2]];
    res.setHeader("Set-Cookie", cookiesToArr);
  }
};
