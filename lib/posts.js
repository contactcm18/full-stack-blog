export const getCategories = () => {
  return new Promise((resolve, reject) => {
    fetch(`${process.env.apiHost}/posts/categories`, {
      method: "GET"
    })
      .then(response =>
        response.status === 200 ? response.json() : reject("Response not 200.")
      )
      .then(categories => resolve(categories))
      .catch(error => reject(error));
  });
};

export const getCategoryPost = async (category, slug) =>
  new Promise(function(resolve, reject) {
    fetch(
      `${process.env.apiHost}/posts/getPost?category=${category}&slug=${slug}`,
      {
        method: "GET",
        mode: "cors"
      }
    )
      .then(async response => {
        if (response.status !== 200) return reject();
        const responseJson = await response.json();
        return resolve(responseJson);
      })
      .catch(() => {
        return reject();
      });
  });
