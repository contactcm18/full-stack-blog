export const navLinks = [
  { title: "Sign in", href: "/login", auth: false },
  { title: "Post", href: "/post", auth: true },
  { title: "Profile", href: "/profile", auth: true },
  { title: "Browse", href: "/browse" }
];
