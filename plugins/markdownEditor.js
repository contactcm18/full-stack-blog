/// https://github.com/nhn/tui.editor/tree/master/apps/vue-editor

import Vue from "vue";

import "codemirror/lib/codemirror.css";
import "@toast-ui/editor/dist/toastui-editor.css";

import { Editor } from "@toast-ui/vue-editor";

Vue.component("MarkdownEditor", Editor);
