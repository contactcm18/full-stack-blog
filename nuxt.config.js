const fetch = require("node-fetch");

const HOST = process.env.HOST_NAME
  ? "https://" + process.env.HOST_NAME
  : "http://localhost:3000";

const API_HOST = process.env.API_HOST || "http://localhost:4000";

const title = "Blogger";
const description =
  "A place to write posts about anything! Create a blog, write about technology, you name it. All for free.";
const imagePath = "/images/thumbnail.jpg";

export default {
  build: {
    extractCSS: process.env.NODE_ENV === "production",
    optimizeCSS: true
  },

  mode: "universal",

  target: "server",

  env: {
    baseURL: HOST,
    apiHost: API_HOST,
    baseTitle: title
  },

  head: {
    title,
    meta: [
      process.env.ALLOW_INDEX === "true"
        ? ""
        : { name: "robots", content: "noindex,nofollow" },
      {
        hid: "twitter:card",
        name: "twitter:card",
        content: "summary"
      },
      {
        hid: "twitter:title",
        name: "twitter:title",
        content: title
      },
      {
        hid: "twitter:description",
        name: "twitter:description",
        content: description
      },
      {
        hid: "twitter:url",
        name: "twitter:url",
        content: HOST
      },
      {
        hid: "twitter:image",
        name: "twitter:image",
        content: HOST + imagePath
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/images/icons/logo.svg" }
    ]
  },

  css: ["~/assets/css/main.scss"],

  plugins: [{ src: "~/plugins/markdownEditor.js", mode: "client" }],

  generate: {
    routes(callback) {
      fetch(`${API_HOST}/posts/getAllPosts`, {
        method: "GET",
        mode: "cors"
      })
        .then(response => response.json())
        .then(slugs => callback(null, slugs))
        .catch(callback);
    }
  },

  /// Nuxt.js dev-modules
  buildModules: [
    "@nuxt/typescript-build",

    /// https://github.com/nuxt-community/analytics-module
    // "@nuxtjs/google-analytics",

    /// https://purgecss.com/guides/nuxt.html#nuxt-js-plugin
    [
      "nuxt-purgecss",
      {
        enabled: process.env.NODE_ENV === "production",
        mode: "webpack",
        paths: [
          "components/**/*.vue",
          "layouts/**/*.vue",
          "pages/**/*.vue",
          "sections/**/*.vue",
          "plugins/**/*.js"
        ],
        styleExtensions: [".scss", ".css"],
        whitelist: ["d-none", "container"],
        whitelistPatterns: [/CodeMirror.*/, /cm-.*/, /tui-.*/, /te-.*/]
      }
    ]
  ],

  modules: [
    /// https://pwa.nuxtjs.org/meta
    "@nuxtjs/pwa",

    /// https://www.npmjs.com/package/@nuxtjs/recaptcha
    // "@nuxtjs/recaptcha",

    "@nuxtjs/style-resources",

    /// https://www.npmjs.com/package/@nuxtjs/sitemap
    "@nuxtjs/sitemap",

    /// https://www.npmjs.com/package/nuxt-webfontloader
    "nuxt-webfontloader"
  ],

  // googleAnalytics: {
  //   id: process.env.GOOGLE_ANALYTICS_ID || "null"
  // },

  pwa: {
    meta: {
      name: title,
      author: "Charles Mattox",
      description,
      ogType: "website",
      ogSiteName: "Blogger",
      ogTitle: title,
      ogDescription: description,
      ogHost: HOST,
      ogImage: imagePath
    }
  },

  // recaptcha: {
  //   hideBadge: true, // Hide badge element (v3 & v2 via size=invisible)
  //   siteKey: process.env.RECAPTCHA_SITE_KEY, // Site key for requests
  //   version: 2, // Version
  //   size: "invisible" // Size: 'compact', 'normal', 'invisible' (v2)
  // },

  styleResources: {
    scss: ["~/assets/css/variables.scss"]
  },

  sitemap: {
    hostname: HOST,
    gzip: true,
    path: "/sitemap.xml",
    trailingSlash: true
  },

  webfontloader: {
    custom: {
      families: ["Dosis:n4,n7"],
      urls: [
        "https://fonts.googleapis.com/css?family=Dosis:400,700&display=swap"
      ]
    }
  }
};
