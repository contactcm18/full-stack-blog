# Some reminders:

- Edit the package.json file (name value)
- Go through nuxt.config.js thoroughly
- Not using Netlify functions? Delete functions/ , netlify.toml, .netlify/

- env variables:
  - HOST_NAME // https://example.com
  - GOOGLE_ANALYTICS_ID
  - MAIL_PASS // sendinblue
  - AUTH_SECRET // recaptcha
